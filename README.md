Flexpass
========

A simple yet flexible library (and command-line application) to access passwords from various backends (GPG/pass, Linux SecretService, Windows Credentials Manager, etc).

See [full documentation](https://flexpass.readthedocs.io/) and [API reference](https://flexpass.readthedocs.io/en/latest/api-reference.html) on _Read the Docs_.


## Installation

Flexpass package is [available on PyPI.org](https://pypi.org/project/flexpass/):

```sh
pip install flexpass
```


## Usage

Flexpass may be used as a library in your Python code:

```py
import flexpass
flexpass.list_passwords()
flexpass.set_password(name, password)
flexpass.get_password(name)
flexpass.delete_password(name)
```

Flexpass may also be invoked as a command-line application (`flexpass` executable is installed with the package):

```sh
flexpass --help
```

Flexpass may also be invoked as a Python module:

```sh
python -m flexpass --help
```


## Main features

This library provides read and write access to passwords, identified by a name, through the following functions:

```py
def get_password(name: str) -> str|None:
    ...

def set_password(name: str, password: str, **options) -> None:
    ...

def delete_password(name: str) -> bool:
    ...

def list_passwords() -> list[PasswordInfo]:
    ...
```

Function `get_password` scans all registered backends, ordered by decreasing priority, and returns the first password found with the given name.

Function `set_password` sets the given password in the writable backend with the highest priority.

Function `delete_password` deletes the given password in all writable backends where the password was set. Returns `True` if the password was deleted from at least one backend, `False` otherwise.

Function `list_passwords` lists all available passwords.

Access to passwords is also possible for a specific backend, by using this backend method. Examples:

```py
backend = get_backend('gpg')
backend.get_password('my/password')
```

Example of invokation of `flexpass` command-line application with the `list` command (which is the default command when no argument is given).
It retrives passwords using function `list_passwords` and format the results:

![Result of `--list` command (example)](https://gitlab.com/ipamo/flexpass/-/raw/main/docs/static/list-result.png "Result of `--list` command (example)")

N.B.: by default, names are displayed truncated to 40 characters. Add option `--full` to disable truncation.


## Backends

The following backends are included in Flexpass package:

| Name            | Priority | Description  |
|--------------------------|----------|--------------|
| `gpg`           | 80       | [GPG](https://www.gnupg.org/) encrypted files following the layout defined by [pass](https://www.passwordstore.org/), the _standard unix password manager_. |
| `secretservice` | 60       | [Secret Service D-Bus API](https://www.freedesktop.org/wiki/Specifications/secret-storage-spec/), a _FreeDesktop.org_ standard usually accessed through [libsecret](https://wiki.gnome.org/Projects/Libsecret) and its frontends ([secret-tool](https://manpages.debian.org/bookworm/libsecret-tools/secret-tool.1.en.html), [Gnome Keyring](https://wiki.gnome.org/Projects/GnomeKeyring), [KDE Wallet Manager](https://apps.kde.org/fr/kwalletmanager5/) or [KeyPassXC](https://keepassxc.org/)). |
| `wincred`       | 50       | [Windows Credential Manager](https://support.microsoft.com/en-us/windows/accessing-credential-manager-1b5c916a-6a16-889f-8581-fc16e8165ac0), the password manager integrated in Windows. |

These backends are planned to be added later (ROADMAP):

| Name            | Priority | Description  |
|--------------------------|----------|--------------|
| `dockersecrets` | 30       | [Docker Compose secrets](https://docs.docker.com/compose/use-secrets/), made available in the containers at path `/run/secrets/{name}` or in `/run/secrets/misc` (or at local path `secrets/{name}.txt` or in `secrets/misc.ini`: usefull during development). **Read-only**. |
| `dir`           | 20       | Unencrypted files in a root directory (by default `/root/passwords`). |
| `env`           | 10       | Environment variables. **Read-only**. |

Custom backends may be registered in your Python code using the `register_backend` function. Example:

```py
from flexpass import register_backend
register_backend(MyCustomBackend, priority=70)
```

Pre-defined priorities may be modified by calling the `register_backend` with the new `priority` value. Example:

```py
register_backend(WincredBackend, priority=5, replace_if_exists=True)
```


## License

This project is licensed under the terms of the [MIT license](https://gitlab.com/ipamo/flexpass/-/raw/main/LICENSE.txt).


## Credits

Inspired by:

- [pass](https://www.passwordstore.org/): the _standard unix password manager_ based on GPG. It has very few requirements and may be installed on most workstations and servers. It is cross-platform because only GPG is actually required to read or write passwords.
- [keyring](https://github.com/jaraco/keyring): a most wildly used Python library to access passwords.

Logo based on a creation by _Pixel perfect_ on [Flaticon](https://www.flaticon.com/free-icons/open-source).
