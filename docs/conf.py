#
# Configuration file for the Sphinx documentation builder.
#
# Inclusion of README.md: see https://stackoverflow.com/questions/46278683/include-my-markdown-readme-into-sphinx/68005314#68005314
#

import os, sys
sys.path.insert(0, os.path.abspath('..'))

from datetime import date

RESET = '\033[0m'
YELLOW = '\033[0;33m'

project = "Flexpass"
copyright = f"2024{f'-{date.today().year}' if date.today().year > 2024 else ''}, Ipamo"
author = "Ipamo"

extensions = [
    'myst_parser',
    'sphinx.ext.autodoc',
    'sphinx.ext.autosummary',
    'sphinx_rtd_theme',
]

templates_path = ['templates']
exclude_patterns = ['build', 'Thumbs.db', '.DS_Store']

html_static_path = ['static']
html_logo = "static/logo-100x100.png"

html_theme = 'sphinx_rtd_theme'
html_context = {
    'display_version': True, # appears under the logo on the left menu
}

autosummary_generate = True
autosummary_ignore_module_all = False
autosummary_imported_members = True
