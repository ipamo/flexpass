```{include} ../README.md
```

```{toctree}
:maxdepth: 1
:caption: Info
Getting started <getting-started>
compare-with-keyring
```

```{toctree}
:maxdepth: 1
:caption: Reference
api-reference
```
