Comparison with `keyring` package
=================================

[keyring](https://github.com/jaraco/keyring) is a most wildly used Python library to access passwords.

Notable differences between `flexpass` and `keyring`:

- `keyring` is a lot more mature and has many more backends.
- `flexpass` does not require to specify a _username_ in addition to the password name.
- `flexpass` provides a predictible and easily configurable order of precedence for the backends.
- `flexpass` aims at including usual backends without requiring additional packages (in particular, the simple `pass` command is avaible).
- `flexpass` easily gives access to the list of passwords.
