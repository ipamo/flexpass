from __future__ import annotations
import os
import sys

from flexpass import BackendNotAvailable
from flexpass.backends.secretservice import SecretServiceBackend

from . import AbstractCases


class Case(AbstractCases.BackendCase):
    backend_cls = SecretServiceBackend

    def test_available(self):
        excepted_details = None
        if sys.platform == 'linux':
            if 'DBUS_SESSION_BUS_ADDRESS' in os.environ:
                super().test_available()
                return
            else:
                excepted_details = 'environment variable DBUS_SESSION_BUS_ADDRESS missing'
        else:
            excepted_details = 'package secretstorage missing'
        
        self.assertIsInstance(self.exception, BackendNotAvailable)
        self.assertEqual(self.exception.backend, 'secretservice')
        self.assertEqual(str(self.exception), f'Backend secretservice not available: {excepted_details}')
