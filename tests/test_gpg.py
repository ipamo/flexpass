from __future__ import annotations
from pathlib import Path
from secrets import token_hex

from flexpass import BackendNotAvailable, register_backend, unregister_backend, get_backend
from flexpass.backends.gpg import GpgBackend

from . import AbstractCases, SAMPLES_DIR


class Case(AbstractCases.BackendCase):
    backend_cls = GpgBackend

    def test_available(self):
        path = Path('~/.password-store').expanduser()
        if path.exists():
            super().test_available()
        else:
            self.assertIsInstance(self.exception, BackendNotAvailable)
            self.assertEqual(self.exception.backend, 'gpg')
            self.assertEqual(str(self.exception), f'Backend gpg not available: GPG password store not found: {path}')

    def test_altkeyring(self):
        try:
            register_backend(GpgBackend, 'gpg:1', store=SAMPLES_DIR.joinpath('gpg-store1'), key=SAMPLES_DIR.joinpath('gpg-store1-key.asc'))
            register_backend(GpgBackend, 'gpg:2', store=SAMPLES_DIR.joinpath('gpg-store2'), key=SAMPLES_DIR.joinpath('gpg-store2-key.asc'))

            for backend_name in ['gpg:1', 'gpg:2']:
                backend = get_backend(backend_name)
                self.assertEqual('world', backend.get_password('hello'))
                self.assertEqual('Fête\nNewLine', backend.get_password('special'))
                
                name = f'local/{token_hex(16)}'
                self.assertEqual(False, backend.delete_password(name))
                self.assertEqual(None, backend.get_password(name))
                try:
                    self.assertEqual(True, backend.set_password(name, 'v1'))
                    self.assertEqual('v1', backend.get_password(name))
                    self.assertEqual(False, backend.set_password(name, 'v2-é-♥'))
                    self.assertEqual('v2-é-♥', backend.get_password(name))
                finally:
                    self.assertEqual(True, backend.delete_password(name))
                    self.assertEqual(None, backend.get_password(name))

        finally:
            unregister_backend('gpg:1', if_exists=True)
            unregister_backend('gpg:2', if_exists=True)
