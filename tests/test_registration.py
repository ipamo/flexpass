from __future__ import annotations

from unittest import TestCase

from flexpass import get_backend_definitions, get_backend_definition, register_backend, unregister_backend
from flexpass.backends.gpg import GpgBackend
from flexpass.backends.secretservice import SecretServiceBackend
from flexpass.backends.wincred import WincredBackend


class Case(TestCase):
    def test_registration(self):
        def get_backend_classes():
            return [definition.backend_cls for definition in get_backend_definitions()]

        # Default priorities
        self.assertEqual(get_backend_classes(), [GpgBackend, SecretServiceBackend, WincredBackend])
        gpg_priority = get_backend_definition(GpgBackend).priority
        wincred_priority = get_backend_definition(WincredBackend).priority

        # Modify priorities
        register_backend(WincredBackend, priority=gpg_priority, replace_if_exists=True)
        register_backend(GpgBackend, priority=wincred_priority, replace_if_exists=True)
        self.assertEqual(get_backend_classes(), [WincredBackend, SecretServiceBackend, GpgBackend])

        # Disable WincredBackend
        unregister_backend(WincredBackend)
        self.assertEqual(get_backend_classes(), [SecretServiceBackend, GpgBackend])

        # Restore priorities
        register_backend(WincredBackend, priority=wincred_priority)
        register_backend(GpgBackend, priority=gpg_priority, replace_if_exists=True)
        self.assertEqual(get_backend_classes(), [GpgBackend, SecretServiceBackend, WincredBackend])
