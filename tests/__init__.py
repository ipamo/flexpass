from __future__ import annotations

import logging
from pathlib import Path
from secrets import token_hex
from unittest import TestCase, skipIf

from flexpass import Backend, BackendNotAvailable, register_backend, get_backend
from flexpass.backends.gpg import GpgBackend
from flexpass.utils import configure_logging

configure_logging()

logger = logging.getLogger(__name__)
logger.debug("Configure tests")

SAMPLES_DIR = Path(__file__).parent.joinpath('samples')

class AbstractCases:
    class BackendCase(TestCase):
        backend_cls: type[Backend]
        backend: Backend = None
        exception: Exception = None

        @classmethod
        def setUpClass(cls):
            try:
                cls.backend = get_backend(cls.backend_cls)
            except Exception as err:
                cls.exception = err

        def checkBefore(self):
            if isinstance(self.exception, BackendNotAvailable):
                self.skipTest(self.exception.reason)
            elif self.exception:
                self.fail(f"{self.exception.__class__.__name__}: {self.exception}")

        def test_available(self):
            self.assertIsInstance(self.backend, self.backend_cls)

        def test_crud(self):
            self.checkBefore()

            name = f'flexpass-tests/{self.backend.name}/{token_hex(16)}'
            self.assertEqual(False, self.backend.delete_password(name))
            self.assertEqual(None, self.backend.get_password(name))
            try:
                self.assertEqual(True, self.backend.set_password(name, 'v1'))
                self.assertEqual('v1', self.backend.get_password(name))
                self.assertEqual(False, self.backend.set_password(name, 'v2-é-♥'))
                self.assertEqual('v2-é-♥', self.backend.get_password(name))
            finally:
                self.assertEqual(True, self.backend.delete_password(name))
                self.assertEqual(None, self.backend.get_password(name))
