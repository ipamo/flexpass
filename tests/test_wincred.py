from __future__ import annotations

import sys

from flexpass import BackendNotAvailable
from flexpass.backends.wincred import WincredBackend

from . import AbstractCases


class Case(AbstractCases.BackendCase):
    backend_cls = WincredBackend

    def test_available(self):
        if sys.platform == 'win32':
            super().test_available()
        else:
            self.assertIsInstance(self.exception, BackendNotAvailable)
            self.assertEqual(self.exception.backend, 'wincred')
            self.assertEqual(str(self.exception), 'Backend wincred not available: package win32cred missing')
